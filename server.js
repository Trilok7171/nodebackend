const express = require('express'),
path = require('path'),
cors = require('cors'),
multer = require('multer'),
bodyparser = require('body-parser');
const {MongoClient} = require('mongodb');
const dbConfig = require('./config/dbConfig.js');
const userRout = require('./router/user')
const loginRout = require('./router/login')




// Express settings
const app = express();
app.use(cors());


app.use(express.json());
app.use('/user', userRout)
app.use('/login', loginRout)
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended:false }));

// create connection
const client = new MongoClient(dbConfig.url, { useNewUrlParser: true , useUnifiedTopology: true});
client.connect(function(err, client) {
 if(err) {
   console.log(err)
 }
 else {
 console.log("Connected correctly to server");
   app.locals.db = client;
  //  console.log(client);
 }
 
});

// Create PORT
const PORT = process.env.PORT || 3000;
const server = app.listen(PORT, () => {
  console.log('Connected to port ' + PORT)
})

// Find 404 and hand over to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  console.error(err.message);
  if (!err.statusCode) err.statusCode = 500;
  res.status(err.statusCode).send(err.message);
});
