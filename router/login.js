var express = require('express');
var router = express.Router();
var md5 = require('md5');


//  login function
	router.post('/',function(req,res,next){
		let client = req.app.locals.db;
	    const db = client.db('TEST');
        // console.log(req.body);
        if(req.body.password == undefined || req.body.username == undefined) {
            return res.status(400).send({
                error:true,
                message: 'Invailid credential.',
                data: []
          });
        }
        var password = md5(req.body.password);
        // let query = {username: req.body.username, password: req.body.password}
        let query = {$and : [{password: password}, {$or : [{"email" : req.body.username},
        {"username" : req.body.username}] }] }
        console.log(query);
        db.collection('USER').find(query).toArray(function(err, result) {
	      if(err) {
	      	return res.status(500).send({
	              error:false,
	              message: 'Some error occured.',
	              data:result
	        });
	      }
            if(result && result.length) { 
                if (result[0]['role'] == 'admin' 
                    || result[0]['role'] == 'super_admin'){
                        res.status(200).send({
                          error:false,
                          message:'login succesfull.',
                          data:result
                        });
                    } else {
                        res.status(200).send({
                            error: true,
                            message:'User can not login to dashbaord.',
                            data:[]
                        }); 

                    }
	        } else {
	            res.status(200).send({
	                error: true,
	                message:'Invailid username or password.',
	                data:[]
	            }); 
	        }
	 	});
	});


 module.exports = router;
