var express = require('express');
var router = express.Router();
const ObjectId = require('mongodb').ObjectID;
var md5 = require('md5');

//  get all users record.
	router.get('/',function(req,res,next){
		let client = req.app.locals.db;
	    const db = client.db('TEST');
				
	   db.collection('USER').find().toArray(function(err, result) {
	      if(err) {
	      	return res.status(500).send({
	              error:false,
	              message: 'Some error occured',
	              data:result
	        });
	      }
	        if(result){
	            res.status(200).send({
	              error:false,
	              message:'',
	              data:result
	            });
	        } else {
	            res.status(200).send({
	                error:true,
	                message:'',
	                data:[]
	            }); 
	        }
	 	});
	});


    // get single user Data.
	router.get('/:id',function(req,res,next){
		let client = req.app.locals.db;
	    const db = client.db('TEST');
		if(!req.params.id) {
            return res.status(400).send({
                error:true,
                message: 'Invailid parameters.',
                data:result
          });
        }		
	   db.collection('USER').find({_id: new ObjectId(req.params.id)}).toArray(function(err, result) {
	      if(err) {
	      	return res.status(500).send({
	              error:false,
	              message: 'Some error occured',
	              data:result
	        });
	      }
	        if(result){
	            res.status(200).send({
	              error:false,
	              message:'',
	              data:result
	            });
	        } else {
	            res.status(200).send({
	                error:true,
	                message:'',
	                data:[]
	            }); 
	        }
	 	});
    });
    
    router.post('/addUser',function(req,res,next){
		let client = req.app.locals.db;
        const db = client.db('TEST');
        if(Object.keys(req.body).length > 0) {}
        var password = md5(req.body.password);
        req.body['password'] = password;
        if(!req.body['password'] || !req.body['email'] || !req.body['username']) {
            return res.status(400).send({
                error:true,
                message: 'Invailid parameters.',
                data:result
        });
        } else {
            db.collection('USER').find({$or : [{email: req.body.email}, {username: req.body.username}]}).toArray(function(err, result) {
                if(err) {
                    return res.status(500).send({
                            error:false,
                            message: 'Some error occured',
                            data:result
                    });
                }
                if(result && result.length) {
                    console.log(result)
                    if(result[0].email == req.body.email) {
                        res.status(200).send({
                            error:true,
                            message:'Email already exist',
                        });
                       
                    } else {
                        if(result[0].username == req.body.username) {
                            res.status(200).send({
                                error:true,
                                message:'Username already exist',
                            });
                        }
                    }
                } else {
                    db.collection('USER').insertOne(req.body, function(err, result) {
                        if(err) {
                        return res.status(500).send({
                                error:false,
                                message: 'Some error occured',
                                data:result
                        });
                        }
                        if(result){
                            res.status(200).send({
                                error:false,
                                message:'User Created Sussesfully',
                                data:result
                            });
                        } else {
                            res.status(200).send({
                                error:true,
                                message:'Something went Wrong',
                                data:[]
                            }); 
                        }
                        
                    })
    
                }
            })
        }
    });
    
    // update single user.
    router.post('/updateUser/:id',function(req,res,next){
		let client = req.app.locals.db;
        const db = client.db('TEST');
        if(!req.params.id) {
            return res.status(400).send({
                error:true,
                message: 'Invailid parameters',
                data:result
            });
        }
       let object=  {_id: new ObjectId(req.params.id)}
        console.log('object', object);
        db.collection('USER').update(object,{
            $set : req.body
        }, function(err, result) {
            if(err) {
                console.log(err);
            return res.status(500).send({
                    error:false,
                    message: 'Some error occured',
                    data:result
            });
            }
            if(result){
                res.status(200).send({
                    error:false,
                    message:'User updated Sussesfully',
                    data:result
                });
            } else {
                res.status(200).send({
                    error:true,
                    message:'Something went Wrong',
                    data:[]
                }); 
            }
        })
    });
    
    // delete single user.
    router.delete('/deleteUser/:id',function(req,res,next){
		let client = req.app.locals.db;
        const db = client.db('TEST');
        if(!req.params.id) {
            return res.status(400).send({
                error:false,
                message: 'Ivailid parameters.',
                data:result
            });
        }
       let object=  {_id: new ObjectId(req.params.id)}
        db.collection('USER').deleteOne(object, function(err, result) {
            if(err) {
                console.log(err);
            return res.status(500).send({
                    error:false,
                    message: 'Some error occured',
                    data:result
            });
            }
            if(result){
                res.status(200).send({
                    error:false,
                    message:'User deleted Sussesfully',
                    data:result
                });
            } else {
                res.status(200).send({
                    error:true,
                    message:'Something went Wrong',
                    data:[]
                }); 
            }
        })
	});

 module.exports = router;
